package za.ac.kleenbin.utitlity;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.junit.Test;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Business;
import za.ac.kleenbin.adminapplication.model.Invoice;
import za.ac.kleenbin.adminapplication.model.InvoiceCommunication;
import za.ac.kleenbin.adminapplication.model.Payment;

public class AccessDBLoaderTest {

	@Test
	public void test() throws URISyntaxException {

		EntityManager entityManager = mock(EntityManager.class);

		when(entityManager.find(Business.class, 1)).thenReturn(new Business());

		TypedQuery balanceUpdateQuery = mock(TypedQuery.class);

		when(balanceUpdateQuery.getResultList()).thenReturn(new ArrayList());

		when(
				entityManager.createQuery("from Invoice inv "
						+ "where inv.date = :date "
						+ "AND inv.amount = :amount "
						+ "AND inv.account.accountNumber = :accountNumber "
						+ "AND inv.metaData.id != :metaDataId ", Invoice.class))
				.thenReturn(balanceUpdateQuery);

		when(
				entityManager.createQuery("from Invoice inv "
						+ "where inv.date = :date "
						+ "AND inv.amount = :amount "
						+ "AND inv.account.accountNumber = :accountNumber "
						+ "AND inv.metaData.id = :metaDataId ", Invoice.class))
				.thenReturn(balanceUpdateQuery);

		when(
				entityManager.createQuery("from Payment pay "
						+ "where pay.date = :date "
						+ "AND pay.amount = :amount "
						+ "AND pay.account.accountNumber = :accountNumber "
						+ "AND pay.metaData.id != :metadataId ", Payment.class))
				.thenReturn(balanceUpdateQuery);
		when(
				entityManager.createQuery("from Payment pay "
						+ "where pay.date = :date "
						+ "AND pay.amount = :amount "
						+ "AND pay.account.accountNumber = :accountNumber "
						+ "AND pay.metaData.id = :metadataId ", Payment.class))
				.thenReturn(balanceUpdateQuery);

		TypedQuery<Account> accountQuery = mock(TypedQuery.class);
		List<Account> accounts = new ArrayList<Account>();
		accounts.add(new Account());
		when(accountQuery.getResultList()).thenReturn(accounts);
		when(
				entityManager.createQuery(
						"from Account a where a.accountNumber = ?",
						Account.class)).thenReturn(accountQuery);
		Query nativeQuery = mock(Query.class);
		when(nativeQuery.getSingleResult()).thenReturn(new BigInteger("0"));
		when(
				entityManager
						.createNativeQuery("select count(*) from Account a where a.accountNumber = ?"))
				.thenReturn(nativeQuery);

		AccessDBLoader loader = new AccessDBLoader();
		loader.setEntityManager(entityManager);

		URL url = ClassLoader.getSystemResource("KleenBin.mdb");

		File file = new File(url.toURI());

		loader.go(file, false, 2013);

		int countBusiness = 1;
		int countAccount = 323;
		int countInvoice = 3500;
		int countPayment = 1586;
		int countInvoiceCommunication = 1;

		// Business
		verify(entityManager, times(countBusiness))
				.persist(isA(Business.class));

		// Accounts
		verify(entityManager, times(countAccount)).persist(isA(Account.class));

		// Invoice
		verify(entityManager, times(countInvoice)).persist(isA(Invoice.class));

		// Payment
		verify(entityManager, times(countPayment)).persist(isA(Payment.class));

		// Invoice comments
		verify(entityManager, times(countInvoiceCommunication)).persist(
				isA(InvoiceCommunication.class));
	}

}
