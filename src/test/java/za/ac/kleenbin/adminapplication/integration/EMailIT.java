package za.ac.kleenbin.adminapplication.integration;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import javax.mail.internet.AddressException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class EMailIT {

	@Test
	public void test() throws AddressException, IOException {

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", true);
		properties.put("mail.smtp.starttls.enable", true);
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", 587);
		String username = "kleenbinfg@gmail.com";
		String password = "kleenbinfg";
		String from = username;

		//
		EMail eMail = new EMail(from, properties, username, password);

		String to = username;
		String messageText = "testing 1,2,3";
		String subject = "testing 1,2,3";

		URL url = ClassLoader.getSystemResource("test.pdf");

		byte[] byteArray = IOUtils.toByteArray(url.openStream());
		
		//
		eMail.send(new String[] {to}, subject, messageText, eMail.new Attachment(byteArray, "application/pdf", "testFile.pdf"));

		//
	}

}
