package za.ac.kleenbin.adminapplication.service;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Ignore;
import org.junit.Test;

import za.ac.kleenbin.adminapplication.service.impl.MessageInfo;
import za.ac.kleenbin.adminapplication.service.impl.SMSServiceImpl;

public class SMSServiceImplTest {

	@Test
	@Ignore
	public void testSendSMS_live() {
		
		boolean live = true;
		int delay = 5;
		testSendSMS(live, delay);
	}
	
	@Test
	@Ignore
	public void testSendSMS_notLive() {
		
		boolean live = false;
		int delay = 1;
		testSendSMS(live, delay);
	}

	private void testSendSMS(boolean live, int delay) {
		
		SMSServiceImpl service = new SMSServiceImpl();
		
		service.setPassword("bUlksms1");
		service.setUsername("tseliso_KBFG");
		service.setUrl("http://www.mymobileapi.com/api5/http5.aspx");
		service.setLive(live);

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, delay);
		
		service.sendSMS(calendar.getTime(),
				new MessageInfo("0721179872", "ACC-010001", "this is a message 1"),
	           new MessageInfo("0726467948", "ACC-010002", "this is a message 2"));
	}	
	
	@Test
	public void testCreateXML() throws ParseException {
		
		String dateString = "04/Nov/2013";
		String timeString = "08:00";
		
		SMSServiceImpl service = new SMSServiceImpl();
		
		String xml = service.createXML(new SimpleDateFormat(
				"dd/MMM/yyyyHH:mm").parse(dateString + timeString), 
				new MessageInfo("0711060826", "ACC-010001", "this is a message 1"),
				new MessageInfo("0711060827", "ACC-010002", "this is a message 2"));

		//
		assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
				+ "<senddata>\n"
						+ "<settings>\n"
								+ "<live>false</live>\n"
								+ "<default_date>" + dateString + "</default_date>\n"
								+ "<default_time>" + timeString + "</default_time>\n"
						+ "</settings>\n"
						+ "<entries>\n"
							+ "<numto>0711060826</numto>\n"
							+ "<customerid>ACC-010001</customerid>\n"
							+ "<data1>this is a message 1</data1>\n"
						+ "</entries>\n"
						+ "<entries>\n"
						    + "<numto>0711060827</numto>\n"
						    + "<customerid>ACC-010002</customerid>\n"
						    + "<data1>this is a message 2</data1>\n"
						+ "</entries>\n"
					+ "</senddata>\n", xml);
	}

}
