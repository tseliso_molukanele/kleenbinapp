BEGIN TRANSACTION;

INSERT INTO PERSON(TITLE, INITIALS, FIRSTNAME, LASTNAME, ADDRESS_ID)  SELECT '?', '?', '?', '?', MAX(ID) FROM ADDRESS; 
SELECT SET(@PersonID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM PERSON);

INSERT INTO ACCOUNT(ACCOUNTHOLDER_ID, ACCOUNTNUMBER, FREQUENCY, NUMBEROFBINS, DAYOFCLEANING, OUTSTANDINGBALANCE, SALESCONSULTANT, COMMENCEMENTDATE, ACTIVE) SELECT MAX(ID), ?, ?, ?, ?, ?, '?', '?', '?' FROM PERSON;
SELECT SET(@AccountID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM ACCOUNT);

INSERT INTO ADDRESS(ADDRESSLINE1, STREET, SURBURB, CODE) VALUES ('?', '?', '?', '?');
SELECT SET(@AddressId, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM ADDRESS);

UPDATE PERSON SET ADDRESS_ID = @AddressId WHERE ID = @PersonId; 

SELECT SET(@PhoneNumberID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM PHONENUMBER);
INSERT INTO PHONENUMBER (NUMBER, NUMBERTYPE) SELECT num, type FROM (SELECT '?' as num, 1 as type) WHERE num <> '';
INSERT INTO PERSON_PHONENUMBER (PERSON_ID, PHONENUMBERS_ID) SELECT @PersonID, existing FROM (SELECT MAX(ID) as existing FROM PHONENUMBER) WHERE (existing > @PhoneNumberID) OR (not existing is null AND @PhoneNumberID is null);

SELECT SET(@PhoneNumberID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM PHONENUMBER);
INSERT INTO PHONENUMBER (NUMBER, NUMBERTYPE) SELECT num, type FROM (SELECT '?' as num, 2 as type) WHERE num <> '';
INSERT INTO PERSON_PHONENUMBER (PERSON_ID, PHONENUMBERS_ID) SELECT @PersonID, existing FROM (SELECT MAX(ID) as existing FROM PHONENUMBER) WHERE (existing > @PhoneNumberID) OR (not existing is null AND @PhoneNumberID is null);

SELECT SET(@PhoneNumberID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM PHONENUMBER);
INSERT INTO PHONENUMBER (NUMBER, NUMBERTYPE) SELECT num, type FROM (SELECT '?' as num, 0 as type) WHERE num <> '';
INSERT INTO PERSON_PHONENUMBER (PERSON_ID, PHONENUMBERS_ID) SELECT @PersonID, existing FROM (SELECT MAX(ID) as existing FROM PHONENUMBER) WHERE (existing > @PhoneNumberID) OR (not existing is null AND @PhoneNumberID is null);
   
SELECT SET(@EmailAddressID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM EMAILADDRESS);
INSERT INTO EMAILADDRESS (email) SELECT email FROM (SELECT '?' as email) WHERE email <> '';
INSERT INTO PERSON_EMAILADDRESS (PERSON_ID, EMAILADDRESSES_ID) SELECT @PersonID, existing FROM (SELECT MAX(ID) as existing FROM EMAILADDRESS) WHERE (existing > @EmailAddressID) OR (not existing is null AND @EmailAddressID is null);   

SELECT SET(@BusinessID, MAX) RUNNING_TOTAL FROM (SELECT MAX(ID) AS MAX FROM BUSINESS);
UPDATE ACCOUNT SET BUSINESS_ID = @BusinessID WHERE ID = @AccountID;

INSERT INTO SCHEDULECOMMENT (COMMENT, DATE, ACCOUNT_ID) SELECT comment, date, accId FROM (SELECT '?' as comment, NOW() as date, @AccountID as accId) WHERE comment <> ''; 
INSERT INTO INVOICECOMMENT (COMMENT, DATE, ACCOUNT_ID) SELECT comment, date, accId FROM (SELECT '?' as comment, NOW() as date, @AccountID as accId) WHERE comment <> '';
INSERT INTO GENERALCOMMENT (COMMENT, DATE, ACCOUNT_ID) SELECT comment, date, accId FROM (SELECT '?' as comment, NOW() as date, @AccountID as accId) WHERE comment <> '';

COMMIT;
