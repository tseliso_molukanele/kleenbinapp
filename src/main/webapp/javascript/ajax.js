function sendReplacingRequestObject(url, argumentString, optionalSuccessCallback) {
	
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {

			if (xmlhttp.status == 200) {

				try {
					var indexOfColon = xmlhttp.responseText.indexOf(":");
					var replacementId = xmlhttp.responseText.substring(0,
							indexOfColon);
					var replacementText = xmlhttp.responseText
							.substring(indexOfColon + 1);

					var element = document.getElementById(replacementId);

					element.innerHTML = replacementText;

					if (optionalSuccessCallback != null) {
						optionalSuccessCallback();
					}
				} catch (e) {

					alert("error with Ajax Call to url:" + e + url);
				}
			} else {

				alert("error with Ajax Call to url:" + url);
			}
		}
	}

	xmlhttp.send(argumentString);
}

function sendRequestObject(url, argumentString, optionalSuccessCallback) {

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");

	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {

			if (xmlhttp.status == 200) {

				try {

					if (optionalSuccessCallback != null) {
						optionalSuccessCallback();
					}
				} catch (e) {

					alert("error with Ajax Call to url:" + e + url);
				}
			} else {

				alert("error with Ajax Call to url:" + url);
			}
		}
	}

	xmlhttp.send(argumentString);
}
