<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="refresh" content="5;url=<s:url includeParams=" all" />"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/jquery-ui-1.8.21.custom.css"  />
		<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/jquery-ui-1.8.21.custom.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/javascript/ajax.js"></script>
        <title><tiles:insertAttribute name="title" ignore="true" /></title>
    </head>
    <body>
    	<div>
    		<div id="headingDiv"
    			style="position: absolute; left: 10px; top: 15px; width: 100%; text-align: center;">
    			<h1 font-size='13pt'>Practical Exam Booking </h1>
    		</div>
    		<div id="menuDiv" style="position: absolute; left: 10px; top: 45px; width: 139px;">
    			<tiles:insertAttribute name="menu" />
    		</div>
    		<div id="mainDiv"
    			style="position: absolute; left: 139px; top: 45px; right: 0px; width: auto; ">
    			<div id="headerDiv">
    				<tiles:insertAttribute name="header" />
    			</div>
    			<div id="mainDiv">
    				<tiles:insertAttribute name="body" />
    			</div>
    		</div>
    	</div>
    </body>
</html>
<!--  style="position: absolute; left: 10px; top: 45px; width: 139px; -->