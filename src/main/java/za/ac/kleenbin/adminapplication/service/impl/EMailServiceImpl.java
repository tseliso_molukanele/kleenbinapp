package za.ac.kleenbin.adminapplication.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import za.ac.kleenbin.adminapplication.integration.EMail;
import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.EmailAddress;
import za.ac.kleenbin.adminapplication.service.EmailService;
import za.ac.kleenbin.adminapplication.service.TemplateConverter;

@Service
public class EMailServiceImpl implements EmailService {

	private Logger logger = Logger.getLogger(this.getClass());

	private static final DateFormat YYYYMM = new SimpleDateFormat("yyyyMM");
	private static final DateFormat MMMMM_YYYY = new SimpleDateFormat(
			"MMMMM yyyy");
	private static final DateFormat YYYYMMDD = new SimpleDateFormat(
			"yyyy-MM-dd");

	@Autowired
	private EMail emailer;

	@Value(value = "${report.location}")
	private String reportLocation;

	@Autowired
	private TemplateConverter templateConverter;

	public TemplateConverter getTemplateConverter() {
		return templateConverter;
	}

	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}

	@Override
	public void sendLatestInvoice(Account account) {

		Date now = new Date();

		Map<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("account", account);

		byte[] byteArray = null;
		String emailText = null;

		try {

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(reportLocation
					+ "/report/run?__report=invoice.rptdesign&__format=pdf");
			stringBuilder.append("&accountNumber=");
			stringBuilder.append(account.getAccountNumber());
			stringBuilder.append("&date=");
			stringBuilder.append(YYYYMMDD.format(now));
			stringBuilder.append("&message=");
			stringBuilder.append(URLEncoder.encode(templateConverter
					.convertUsingVelocity("template/invoiceMessage.vm",
							attributes), "UTF-8"));

			emailText = templateConverter.convertUsingVelocity(
					"mail/invoice.vm", attributes);

			URL url = new URL(stringBuilder.toString());

			byteArray = IOUtils.toByteArray(url.openStream());

		} catch (MalformedURLException e1) {
			throw new RuntimeException(e1);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		List<EmailAddress> emailAddresses = account.getAccountHolder()
				.getEmailAddresses();
		String[] emails = new String[emailAddresses.size()];

		for (int i = 0; i < emailAddresses.size(); i++) {

			emails[i] = emailAddresses.get(i).getEmail();
		}

		String subject = "Invoice for " + MMMMM_YYYY.format(now)
				+ " for account " + account.getAccountNumber();
		logger.info("Sending: " + subject + ", to: " + emails[0]);
		emailer.send(emails, subject, emailText,
				emailer.new Attachment(byteArray, "application/pdf", "Invoice"
						+ YYYYMM.format(now) + "_" + account.getAccountNumber()
						+ ".pdf"));

	}
}