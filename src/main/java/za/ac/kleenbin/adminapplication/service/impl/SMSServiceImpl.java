package za.ac.kleenbin.adminapplication.service.impl;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.log4j.Logger;
import org.xembly.Directives;
import org.xembly.ImpossibleModificationException;
import org.xembly.Xembler;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.PhoneNumber;
import za.ac.kleenbin.adminapplication.model.PhoneNumber.NumberType;
import za.ac.kleenbin.adminapplication.service.SMSService;
import za.ac.kleenbin.adminapplication.service.TemplateConverter;

public class SMSServiceImpl implements SMSService {

	private static Logger logger = Logger.getLogger(SMSServiceImpl.class);

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"dd/MMM/yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	private static final DateFormat FULL_FORMAT = new SimpleDateFormat(
			"yyyy-MMM-dd HH:mm");

	private TemplateConverter templateConverter;
	private boolean connectlive = false;

	public TemplateConverter getTemplateConverter() {
		return templateConverter;
	}

	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}

	public boolean isConnectlive() {
		return connectlive;
	}

	public boolean getConnectlive() {

		return this.connectlive;
	}

	public void setConnectlive(boolean connectlive) {
		this.connectlive = connectlive;
	}

	@Override
	public void sendSMS(Date time, MessageInfo... messageInfos) {

		try {
			String createdXML = createXML(time, messageInfos);
			String requestString = new StringBuilder(url).append("?Type=")
					.append("send").append("&username=").append(username)
					.append("&password=").append(password).append("&xmldata=")
					.append(URLEncoder.encode(createdXML, "UTF-8")).toString();

			Request getRequest = Request.Get(requestString);

			if (connectlive) {
			
				Content content = getRequest.execute().returnContent();
				logger.debug(content.asString());
			} else {

				logger.debug("connection to SMS system skipped");
			}
			
			logger.trace(" ----- ");
			logger.debug("Sending time: " + FULL_FORMAT.format(time));
			logger.trace(requestString);
			logger.trace(" ~~~~~ ");
			logger.trace(" ===== ");

		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public String createXML(Date time, MessageInfo... messageInfos) {

		Directives senddata = new Directives();

		senddata.add("senddata").add("settings").add("live")
				.set(isLive().toString()).up().add("default_date")
				.set(DATE_FORMAT.format(time)).up().add("default_time")
				.set(TIME_FORMAT.format(time)).up().up();

		for (MessageInfo messageInfo : messageInfos) {

			senddata.add("entries").add("numto").set(messageInfo.getNumber())
					.up().add("customerid").set(messageInfo.getCustomerId())
					.up().add("data1").set(messageInfo.getMessage()).up().up();
		}

		try {
			return new Xembler(senddata).xml();
		} catch (ImpossibleModificationException e) {

			throw new RuntimeException(e);
		}
	}

	private String username;
	private String password;
	private String url;
	private Boolean live = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean isLive() {
		return live;
	}

	public Boolean getLive() {
		return live;
	}

	public void setLive(Boolean live) {
		this.live = live;
	}

	@Override
	public void sendLatestInvoice(Account account) {

		boolean sending = false;
		Iterator<PhoneNumber> iter = account.getAccountHolder()
				.getPhoneNumbers().iterator();
		MessageInfo messageInfo = null;
		while (!sending && iter.hasNext()) {
			PhoneNumber number = iter.next();

			if (number.getNumberType().equals(NumberType.CELL)) {

				Map<String, Object> attributes = new HashMap<String, Object>();
				attributes.put("account", account);

				String message;
				try {
					message = templateConverter.convertUsingVelocity(
							"sms/invoice.vm", attributes);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

				messageInfo = new MessageInfo(number.getNumber(), "ANM-"
						+ account.getAccountNumber(), message);
				sending = true;
			}
		}

		if (!sending) {

			logger.info("no cell number for Acc ID=" + account.getId()
					+ ", Acc Num=" + account.getAccountNumber());
		} else {

			logger.info("Sending - " + messageInfo);

			Date now = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setLenient(false);

			calendar.setTime(now);

			if (calendar.get(Calendar.HOUR_OF_DAY) > 8) {

				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}

			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 30);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			while (!isWeekday(calendar)) {

				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}

			sendSMS(calendar.getTime(), messageInfo);
		}
	}

	private boolean isWeekday(Calendar calendar) {

		int dow = calendar.get(Calendar.DAY_OF_WEEK);
		boolean isWeekday = ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));

		return isWeekday;
	}
}
