package za.ac.kleenbin.adminapplication.service;

import java.util.Date;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.service.impl.MessageInfo;

public interface SMSService {

	public abstract void sendSMS(Date time, MessageInfo ... messageInfos);

	public abstract void sendLatestInvoice(Account account);
}