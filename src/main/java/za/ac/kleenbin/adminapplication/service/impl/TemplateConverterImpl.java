package za.ac.kleenbin.adminapplication.service.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import za.ac.kleenbin.adminapplication.service.TemplateConverter;

public class TemplateConverterImpl implements TemplateConverter {

	private VelocityEngine velocityEngine;
	
	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	@Override
	public String convertUsingVelocity(String templateName,
			Map<String, Object> attributes) throws IOException {

		StringWriter result = new StringWriter();

		Template template = velocityEngine.getTemplate(templateName);

		Context context = new VelocityContext();

		context.put("dateTool", new DateTool());
		context.put("numberTool", new NumberTool());

		if (attributes != null) {
			for (String key : attributes.keySet()) {

				context.put(key, attributes.get(key));
			}
		}

		template.merge(context, result);

		result.close();
		return result.toString();
	}

	
}
