package za.ac.kleenbin.adminapplication.service;

import java.io.IOException;
import java.util.Map;

public interface TemplateConverter {

	String convertUsingVelocity(String templateName,
			Map<String, Object> attributes) throws IOException;

}
