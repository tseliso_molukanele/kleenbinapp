package za.ac.kleenbin.adminapplication.service.impl;

public class MessageInfo {

	private String number;
	private String message;
	private String customerId;

	public MessageInfo(String number, String customerId, String message) {
		
		this.number = number;
		this.customerId = customerId;
		this.message = message;
	}

	public String getNumber() {
		return number;
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		
		return number + "~" + message + "~" + customerId;
	}
}
