package za.ac.kleenbin.adminapplication.service;

import za.ac.kleenbin.adminapplication.model.Account;

public interface EmailService {

	void sendLatestInvoice(Account account);
}
