package za.ac.kleenbin.adminapplication.exception;

public class IntegrationException extends RuntimeException {

	private static final long serialVersionUID = -6827845816560928292L;

	public IntegrationException() {
		super();
	}

	public IntegrationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public IntegrationException(String message, Throwable cause) {
		super(message, cause);
	}

	public IntegrationException(String message) {
		super(message);
	}

	public IntegrationException(Throwable cause) {
		super(cause);
	}

}
