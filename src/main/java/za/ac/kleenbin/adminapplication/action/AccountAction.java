package za.ac.kleenbin.adminapplication.action;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.service.TemplateConverter;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage("kleeenDefault")
public class AccountAction extends ActionSupport implements
		ServletResponseAware {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(AccountAction.class);

	private static final String SUCCESS_JSON = "SUCCESS_JSON";

	private static final String JSON = "json";

	private static final DateFormat YYYYMM = new SimpleDateFormat("yyyyMM");
	private static final DateFormat YYYYMMDD = new SimpleDateFormat(
			"yyyy-MM-dd");

	@PersistenceContext
	protected EntityManager entityManager;

	@Autowired
	private TemplateConverter templateConverter;

	@Value(value = "${report.location}")
	private String reportLocation;

	private Long id;

	private String outputFileNameParam;

	private Date date;

	private boolean withEmail;

	private boolean withoutEmail;

	public boolean getWithEmail() {
		return withEmail;
	}

	public boolean getWithoutEmail() {
		return withoutEmail;
	}

	public void setWithEmail(boolean withEmail) {
		this.withEmail = withEmail;
	}

	public void setWithoutEmail(boolean withoutEmail) {
		this.withoutEmail = withoutEmail;
	}

	public Date getDate() {
		if (date == null) {
			return new Date();
		}
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOutputFileNameParam() {
		return outputFileNameParam;
	}

	public void setOutputFileNameParam(String outputFileNameParam) {
		this.outputFileNameParam = outputFileNameParam;
	}

	public Account getAccount() {

		return entityManager.find(Account.class, getId());
	}

	private InputStream inputStream;

	public InputStream getInputStream() {
		return inputStream;
	}

	private String searchString;

	private String viewContext;

	public void setViewContext(String viewContext) {
		this.viewContext = viewContext;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public Map<Long, String> getSearchResults() {

		Query query = entityManager
				.createNativeQuery(" select "
						+ " a.id, "
						+ " concat(a.accountnumber, ' - ',"
						+ " p.firstname, ' ', p.lastname, ' - ',"
						+ " adr.addressline1, ' - ', adr.street, ' - ', adr.surburb, ' - ', adr.code) as RESULT"
						+ " from Account a"
						+ " join Person p on a.accountHolder_Id = p.id"
						+ " join Address adr on adr.id = a.address_id"
						+ " where" + " lower(a.accountnumber) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(p.firstname) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(p.lastname) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(adr.addressline1) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(adr.street) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(adr.surburb) like '%"
						+ this.searchString.toLowerCase() + "%'"
						+ " or lower(adr.code) like '%"
						+ this.searchString.toLowerCase() + "%';");

		@SuppressWarnings("unchecked")
		List<Object[]> queryResults = query.getResultList();

		Map<Long, String> results = new HashMap<Long, String>();
		for (Object[] objects : queryResults) {

			results.put(((BigInteger) objects[0]).longValue(),
					(String) objects[1]);
		}

		return results;
	}

	@Action(value = "account_search", results = { @Result(type = "tiles", name = SUCCESS, location = "/account_search-json.tiles") })
	public String searchAccount() {

		response.addHeader("Access-Control-Allow-Origin", "*");

		return SUCCESS;
	}

	@Action(value = "account_query", results = { @Result(type = "tiles", name = SUCCESS, location = "/account_query.tiles") })
	public String queryAccount() {

		return SUCCESS;
	}

	@Action(value = "account", results = {
			@Result(type = "tiles", name = SUCCESS, location = "/account.tiles"),
			@Result(type = "tiles", name = SUCCESS_JSON, location = "/account-json.tiles"),
			@Result(type = "tiles", name = INPUT, location = "/account_query.tiles") })
	public String displayAccount() {

		response.addHeader("Access-Control-Allow-Origin", "*");
		
		if (JSON.equalsIgnoreCase(viewContext)) {
			return SUCCESS_JSON;
		} else {
			return SUCCESS;
		}
	}

	@Action(value = "captureWash", results = { @Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/washCaptured.tiles") })
	public String captureWash() {

		return SUCCESS;
	}

	@Action(value = "invoice_report", results = {
			@Result(type = "stream", name = ActionSupport.SUCCESS,

			params = { "contentType", "application/pdf", "inputName",
					"inputStream", "contentDisposition",
					"${outputFileNameParam}", "bufferSize", "1024" })

			,
			@Result(type = "tiles", name = ActionSupport.INPUT, location = "/schedule.tiles")

	})
	public String invoiceReport() {

		Account account = entityManager.find(Account.class, getId());
		try {
			Map<String, Object> attributes = new HashMap<String, Object>();
			attributes.put("account", account);

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(reportLocation
					+ "/report/run?__report=invoice.rptdesign&__format=pdf");
			stringBuilder.append("&accountNumber=");
			stringBuilder.append(account.getAccountNumber());
			stringBuilder.append("&date=");
			stringBuilder.append(YYYYMMDD.format(getDate()));
			stringBuilder.append("&message=");
			stringBuilder.append(URLEncoder.encode(templateConverter
					.convertUsingVelocity("template/invoiceMessage.vm",
							attributes), "UTF-8"));

			setOutputFileNameParam("fileName=Invoice"
					+ YYYYMM.format(getDate()) + "_"
					+ account.getAccountNumber() + ".pdf");

			URL url = new URL(stringBuilder.toString());

			inputStream = url.openStream();

		} catch (MalformedURLException e1) {
			throw new RuntimeException(e1);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return SUCCESS;
	}

	@Action(value = "statement_report", results = {
			@Result(type = "stream", name = ActionSupport.SUCCESS,

			params = { "contentType", "application/pdf", "inputName",
					"inputStream", "contentDisposition",
					"${outputFileNameParam}", "bufferSize", "1024" })

			,
			@Result(type = "tiles", name = ActionSupport.INPUT, location = "/schedule.tiles")

	})
	public String statementReport() {

		Account account = entityManager.find(Account.class, getId());

		logger.info("Report location: " + reportLocation);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(reportLocation
				+ "/report/run?__report=statement.rptdesign&__format=pdf");
		stringBuilder.append("&accountNumber=");
		stringBuilder.append(account.getAccountNumber());

		setOutputFileNameParam("fileName=Statement" + YYYYMM.format(new Date())
				+ "_" + account.getAccountNumber() + ".pdf");

		try {
			URL url = new URL(stringBuilder.toString());

			inputStream = url.openStream();

		} catch (MalformedURLException e1) {
			throw new RuntimeException(e1);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return SUCCESS;
	}

	private HttpServletResponse response;

	@Override
	public void setServletResponse(HttpServletResponse response) {

		this.response = response;
	}
}
