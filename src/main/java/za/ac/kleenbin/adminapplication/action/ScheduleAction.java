package za.ac.kleenbin.adminapplication.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.stereotype.Controller;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Schedule;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage("kleeenDefault")
public class ScheduleAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	protected final Log logger = LogFactory.getLog(getClass());

	@PersistenceContext
	protected EntityManager entityManager;

	private InputStream inputStream;

	public InputStream getInputStream() {
		return inputStream;
	}

	@Action(value = "schedule", results = { @Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/schedule.tiles") })
	public String hello() {

		return SUCCESS;
	}

	@Action(value = "schedule_report", results = {
			@Result(type = "stream", name = ActionSupport.SUCCESS,

			params = { "contentType", "application/pdf", "inputName",
					"inputStream", "contentDisposition",
					"filename='schedule.pdf'", "bufferSize", "1024" })

			,
			@Result(type = "tiles", name = ActionSupport.INPUT, location = "/schedule.tiles") })
	public String uploadReport() {

		HttpServletResponse response = ServletActionContext.getResponse();

		ByteArrayOutputStream buffer = null;

		byte[] bytes = null;
		bytes = buffer.toByteArray();
		response.setContentLength(bytes.length);

		if (bytes != null) {
			inputStream = new ByteArrayInputStream(bytes);
		}
		return SUCCESS;
	}

	private Date date = new Date();

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public Schedule getSchedule() {

		Calendar today = Calendar.getInstance();
		if (date != null) {
			today.setTime(date);
		}

		Calendar dateIterator = today;
		List<Date> dates = new ArrayList<Date>();
		for (int i = 0; i < 5; i++) {

			dates.add(dateIterator.getTime());
			dateIterator.add(Calendar.DATE, 7);
		}

		Schedule schedule = new Schedule();

		List<Account> accounts = entityManager
				.createQuery(
						" from Account a WHERE a.dayOfCleaning = :dayOfCleaning order by a.accountHolder.address.surburb, a.accountHolder.address.street, a.accountHolder.address.addressLine1 ",
						Account.class)
				.setParameter("dayOfCleaning", today.get(Calendar.DAY_OF_WEEK))
				.getResultList();

		schedule.setAccounts(accounts);

		schedule.setDates(dates);
		schedule.setDatesToSkip(calculateDatesToSkipMap(accounts, dates));

		return schedule;
	}

	private Map<Account, List<Date>> calculateDatesToSkipMap(
			List<Account> accounts, List<Date> dates) {

		Map<Account, List<Date>> skipDatesMap = new LinkedHashMap<Account, List<Date>>();

		for (Account account : accounts) {

			List<Date> skipDates = new ArrayList<Date>();

			for (Date date : dates) {

				switch (account.getFrequency()) {
				case BI_WEEKLY: {

					if (account.hasBeenServicedLastWeek(date)) {
						skipDates.add(date);
					}
				}
					break;
				case EVERY_WEEK: {

				}
					break;// do nothing
				case ONCE_PER_MONTH: {

					if (account.hasBeenServicedThisMonth(date)) {
						skipDates.add(date);
					}
				}

					break;
				}
			}

			skipDatesMap.put(account, skipDates);
		}

		return skipDatesMap;
	}
}