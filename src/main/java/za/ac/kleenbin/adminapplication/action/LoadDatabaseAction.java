package za.ac.kleenbin.adminapplication.action;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import za.ac.kleenbin.utitlity.AccessDBParser;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@InterceptorRefs({
		@InterceptorRef("fileUploadStack"),
		@InterceptorRef(value = "execAndWait", params = { "delay", "1000",
				"delaySleepInterval", "500" }) })
@ParentPackage("kleeenDefault")
public class LoadDatabaseAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private File accessFile;

	private String userImageContentType;

	private String userImageFileName;

	private boolean historyOnly;

	private int baseYear;

	public boolean getHistoryOnly() {
		return historyOnly;
	}

	public void setHistoryOnly(boolean historyOnly) {
		this.historyOnly = historyOnly;
	}

	public int getBaseYear() {
		return baseYear;
	}

	public void setBaseYear(int baseYear) {
		this.baseYear = baseYear;
	}

	@Autowired
	@Qualifier(value = "hibernateBasedAccessDBLoader")
	private AccessDBParser accessDBParser;

	private static Logger logger = Logger.getLogger(LoadDatabaseAction.class);

	@Action(value = "load-database", results = {
			@Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/message.tiles"),
			@Result(type = "tiles", name = "wait", location = "/wait.tiles"),
			@Result(type = "tiles", name = ActionSupport.ERROR, location = "/error.tiles") })
	public String execute() {

		logger.debug("execute()");
		try {
			accessDBParser.go(accessFile, getHistoryOnly(), baseYear);
		} catch (Exception e) {
			logger.error(e);
			setError(e.getMessage());
			return ERROR;
		}

		return SUCCESS;
	}

	private String error;

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	@Action(value = "load-database-input", results = { @Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/loaddatabase.tiles") })
	public String input() {

		return SUCCESS;
	}

	public String getMessage() {

		return accessFile.length() + " file size successfully uploaded";
	}

	public File getAccessFile() {
		return accessFile;
	}

	public void setAccessFile(File accessFile) {
		this.accessFile = accessFile;
	}

	public String getUserImageContentType() {
		return userImageContentType;
	}

	public void setUserImageContentType(String userImageContentType) {
		this.userImageContentType = userImageContentType;
	}

	public String getUserImageFileName() {
		return userImageFileName;
	}

	public void setUserImageFileName(String userImageFileName) {
		this.userImageFileName = userImageFileName;
	}
}
