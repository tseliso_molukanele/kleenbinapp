package za.ac.kleenbin.adminapplication.action;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.stereotype.Controller;

import za.ac.kleenbin.adminapplication.model.Person;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage("kleeenDefault")
public class ContactsAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	protected final Log logger = LogFactory.getLog(getClass());

	@PersistenceContext
	protected EntityManager entityManager;

	@Action(value = "contacts", results = { @Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/contacts.tiles") })
	public String hello() {

		return ActionSupport.SUCCESS;
	}

	public List<Person> getPersons() {

		return entityManager.createQuery(" select p from Person p ",
				Person.class).getResultList();
	}
}