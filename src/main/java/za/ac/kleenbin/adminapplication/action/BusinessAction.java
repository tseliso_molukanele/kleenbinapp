package za.ac.kleenbin.adminapplication.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Business;
import za.ac.kleenbin.adminapplication.service.EmailService;
import za.ac.kleenbin.adminapplication.service.SMSService;
import za.ac.kleenbin.adminapplication.service.TemplateConverter;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage("kleeenDefault")
public class BusinessAction extends ActionSupport implements
		ServletResponseAware {

	private static final long serialVersionUID = 1L;

	private static final String SUCCESS_JSON = "SUCCESS_JSON";

	private static Logger logger = Logger.getLogger(BusinessAction.class);

	@Autowired
	private TemplateConverter templateConverter;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SMSService smsService;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public SMSService getSmsService() {
		return smsService;
	}

	public void setSmsService(SMSService smsService) {
		this.smsService = smsService;
	}

	@Action(value = "business", results = {
			@Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/business.tiles"),
			@Result(type = "tiles", name = SUCCESS_JSON, location = "/business-json.tiles") })
	public String view() {

		if (viewContext != null && viewContext.equalsIgnoreCase("json")) {

			return SUCCESS_JSON;
		} else {
			return SUCCESS;
		}
	}

	@Action(value = "sendInvoices", results = {
			@Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/invoicesSent.tiles"),
			@Result(type = "tiles", name = "wait", location = "/wait.tiles") })
	public String emailInvoices() {

		TypedQuery<Account> query = entityManager.createQuery(
				" from Account a where a.active = true "
				// + " AND a.id > 71 "
						+ " order by a.accountNumber asc ", Account.class);

		List<Account> accounts = query.getResultList();

		sendNormalInvoice(accounts);

		return SUCCESS;
	}

	private void sendNormalInvoice(List<Account> accounts) {

		for (Account account : accounts) {

			if (account.getAccountHolder().getEmailAddresses().size() <= 0) {

				smsService.sendLatestInvoice(account);
			} else {

				if (account.getBalance() > 500) {

					smsService.sendLatestInvoice(account);
				}

				emailService.sendLatestInvoice(account);
			}
		}
	}

	@Action(value = "stats", results = { @Result(type = "tiles", name = SUCCESS, location = "/stats-json.tiles") })
	public String stats() {

		response.addHeader("Access-Control-Allow-Origin", "*");

		return SUCCESS;
	}

	@Action(value = "incomeHistory", results = { @Result(type = "tiles", name = SUCCESS, location = "/incomeHistory-json.tiles") })
	public String incomeHistory() {

		return SUCCESS;
	}

	/*
	 * action properties
	 */
	private String viewContext;

	public void setViewContext(String viewContext) {
		this.viewContext = viewContext;
	}

	public Business getBusiness() {

		return entityManager.find(Business.class, (long) 1);
	}

	public List<Map<String, String>> getStatsLists() {

		List<Map<String, String>> statsList = new ArrayList<Map<String, String>>();

		Map<String, String> stats = new HashMap<String, String>();

		stats.put("Trading as", "Before Sunset");
		stats.put("Since", "2012/05/01");
		stats.put("Number of Contracts", "227");
		stats.put("Total Bins", "310");
		stats.put("Contracts Today", "43");
		stats.put("Bins Today", "47");

		statsList.add(stats);

		stats = new HashMap<String, String>();
		stats.put("Total Washes this Month", "270");
		stats.put("Income this Month", "R 9001.00");
		stats.put("Total Money Owed", "R 10 000.00");
		stats.put("Royalties this Month", "R 1450.00");

		statsList.add(stats);

		return statsList;

		// [ [ {
		// key : "Number of Franchises",
		// value : "101"
		// }, {
		// key : "Average Contracts per Client",
		// value : "300"
		// } ], [ {
		// key : "Total Royalties This Month",
		// value : "R 15 000.00"
		// }, {
		// key : "Pine Oil Sales",
		// value : "R 17 500.00"
		// }, {
		// key : "Soap Sales",
		// value : "R 6 800.00"
		// } ] ]);
	}

	public List<Map<String, String>> getIncomeHistory() {

		logger.debug("Starting");

		Query query = entityManager
				.createNativeQuery(

				" select invoiced.year, invoiced.month, invoiced.invoiced, payed.payed, payed.payed - invoiced.invoiced as difference "
						+ " from (select year(pay.date) as year, month(pay.date) as month, sum(pay.amount) as payed "
						+ "     from Payment pay "
						+ "     group by year(pay.date), month(pay.date) "
						+ "     order by year(pay.date) desc, month(pay.date) desc) as payed "
						+ " join (select year(inv.date) as year, month(inv.date) as month, sum(inv.amount) as invoiced "
						+ "     from Invoice inv "
						+ "     group by year(inv.date), month(inv.date) "
						+ "     order by year(inv.date) desc, month(inv.date) desc) as invoiced "
						+ " on invoiced.month = payed.month "
						+ " and invoiced.year = payed.year " + " limit 12; ");

		@SuppressWarnings("unchecked")
		List<Object[]> queryResults = query.getResultList();

		List<Map<String, String>> incomeHistory = new ArrayList<Map<String, String>>();

		int i = queryResults.size() - 1;
		for (; i >= 0; i--) {

			Object[] objects = queryResults.get(i);

			Map<String, String> hist = new HashMap<String, String>();
			hist.put("month", "" + ((Integer) objects[1] - 1));
			hist.put("invoiced", objects[2].toString());
			hist.put("received", objects[3].toString());
			hist.put("difference", objects[4].toString());
			incomeHistory.add(hist);
		}

		logger.debug("done");
		return incomeHistory;
	}

	private Date date;

	public Date getDate() {
		if (date == null) {
			return new Date();
		}
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getAccountNumbers() {

		Query query = entityManager
				.createNativeQuery("select acc.accountNumber "
						+ "from  Account acc "
						+ "join Person p on p.ID = acc.accountHolder_ID "
						+ "join Address addr on addr.ID = acc.address_ID "
						+ "where acc.active "
						+ "order by dayOfCleaning, addr.surburb, addr.addressLine1, addr.street;");

		@SuppressWarnings("unchecked")
		List<String> queryResults = query.getResultList();

		List<String> results = new ArrayList<String>();

		for (String objects : queryResults) {

			results.add(objects);
		}

		return results;
	}

	private InputStream inputStream;

	public InputStream getInputStream() {
		return inputStream;
	}

	private String outputFileNameParam;

	public String getOutputFileNameParam() {
		return outputFileNameParam;
	}

	public void setOutputFileNameParam(String outputFileNameParam) {
		this.outputFileNameParam = outputFileNameParam;
	}

	private HttpServletResponse response;

	@Override
	public void setServletResponse(HttpServletResponse response) {

		this.response = response;
	}

}
