package za.ac.kleenbin.adminapplication.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;

@Controller
@ParentPackage("kleeenDefault")
public class IndexAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	@Action(value = "index", results = { @Result(type = "tiles", name = ActionSupport.SUCCESS, location = "/index.tiles") })
	public String go() {

		return SUCCESS;
	}
}
