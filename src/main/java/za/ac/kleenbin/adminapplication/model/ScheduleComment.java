package za.ac.kleenbin.adminapplication.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ScheduleComment")
public class ScheduleComment extends Comment {

	private static final long serialVersionUID = 1L;

}
