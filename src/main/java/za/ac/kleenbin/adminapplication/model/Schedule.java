package za.ac.kleenbin.adminapplication.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Schedule implements Serializable {

	private static final long serialVersionUID = -8306530248009466330L;
	private List<Date> dates;
	private List<Account> accounts;
	private Map<Account, List<Date>> datesToSkip;

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	public void setDates(List<Date> dates) {
		this.dates = dates;
	}

	public List<Date> getDates() {
		return dates;
	}

	public void setDatesToSkip(Map<Account, List<Date>> datesToSkip) {
		this.datesToSkip = datesToSkip;
	}

	public Map<Account, List<Date>> getDatesToSkip() {
		return datesToSkip;
	}

	public int countBins() {

		int result = 0;

		if (accounts != null) {

			for (Account account : accounts) {

				result += account.getNumberOfBins();
			}

		}
		return result;
	}
}
