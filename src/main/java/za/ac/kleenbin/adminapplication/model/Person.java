package za.ac.kleenbin.adminapplication.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Person extends Contactable {

	private static final long serialVersionUID = 1L;

	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private String initials;
	@Column
	private String title;
	@OneToMany(mappedBy = "accountHolder")
	private List<Account> accounts;

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDisplayName() {

		String returnValue = "";

		returnValue += (title != null && !title.trim().equals("") ? title + " "
				: "");
		returnValue += (firstName != null && !firstName.trim().equals("") ? firstName
				+ " "
				: "");
		returnValue += (lastName != null && !lastName.trim().equals("") ? lastName
				: "");

		return returnValue;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
}
