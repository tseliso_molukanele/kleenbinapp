package za.ac.kleenbin.adminapplication.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class PhoneNumber extends Model {

	private static final long serialVersionUID = 6324130037991474131L;

	public enum NumberType {
		CELL, HOME, WORK, FAX
	}

	@Column
	private NumberType numberType;

	@Column
	private String number;

	public String getNumber() {
		return number;
	}

	public NumberType getNumberType() {
		return numberType;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setNumberType(NumberType numberType) {
		this.numberType = numberType;
	}

	@Override
	public String toString() {

		return super.toString().charAt(0)
				+ super.toString().substring(1).toLowerCase();
	}
}
