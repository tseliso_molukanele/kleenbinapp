package za.ac.kleenbin.adminapplication.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.time.DateUtils;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "accountNumber"))
public class Account extends Model {

	private static final long serialVersionUID = -4362261780516872575L;

	@OneToOne
	private Address address;

	@ManyToOne
	private Business business;
	@ManyToOne
	private Person accountHolder;
	@Column
	private Integer numberOfBins;
	@Column
	private FREQUENCY frequency;
	@Column
	private String accountNumber;
	@OneToMany(mappedBy = "account")
	private List<Service> services = new ArrayList<Service>();
	@Column
	private int dayOfCleaning;
	@Column
	private float outstandingBalance;
	
	@Column
	private String salesConsultant;
	@Column
	private Date commencementDate;
	@Column
	private boolean active;
	@OneToMany(mappedBy = "account")
	private List<Invoice> invoices;
	@OneToMany(mappedBy = "account")
	private List<Payment> payments;

	@OneToMany(mappedBy = "account")
	private List<ScheduleComment> scheduleComments = new ArrayList<ScheduleComment>();

	@OneToMany(mappedBy = "account")
	private List<GeneralComment> generalComments = new ArrayList<GeneralComment>();

	@OneToMany(mappedBy = "account")
	private List<InvoiceComment> invoiceComments = new ArrayList<InvoiceComment>();

	public float getOutstandingBalance() {
		return outstandingBalance;
	}

	public void setOutstandingBalance(float outstandingBalance) {
		this.outstandingBalance = outstandingBalance;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	
	public String getPaymentReference() {
		return "JX" + String.format("%1$" + 3 + "s", accountNumber).replace(" ", "0");
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

	public List<GeneralComment> getGeneralComments() {
		return generalComments;
	}

	public void setGeneralComments(List<GeneralComment> generalComments) {
		this.generalComments = generalComments;
	}

	public List<InvoiceComment> getInvoiceComments() {
		return invoiceComments;
	}

	public void setInvoiceComments(List<InvoiceComment> invoiceComments) {
		this.invoiceComments = invoiceComments;
	}

	public List<ScheduleComment> getScheduleComments() {
		return scheduleComments;
	}

	public void setScheduleComments(List<ScheduleComment> scheduleComments) {
		this.scheduleComments = scheduleComments;
	}

	public void setAccountHolder(Person accountHolder) {

		this.accountHolder = accountHolder;
	}

	public Person getAccountHolder() {
		return accountHolder;
	}

	public Integer getNumberOfBins() {
		return numberOfBins;
	}

	public void setNumberOfBins(Integer numberOfBins) {
		this.numberOfBins = numberOfBins;
	}

	public FREQUENCY getFrequency() {
		return frequency;
	}

	public void setFrequency(FREQUENCY frequency) {
		this.frequency = frequency;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public List<Service> getServices() {
		return services;
	}

	public int getDayOfCleaning() {
		return dayOfCleaning;
	}

	public void setDayOfCleaning(int dayOfCleaning) {
		this.dayOfCleaning = dayOfCleaning;
	}

	public String getSalesConsultant() {
		return salesConsultant;
	}

	public void setSalesConsultant(String salesConsultant) {
		this.salesConsultant = salesConsultant;
	}

	public Date getCommencementDate() {
		return commencementDate;
	}

	public void setCommencementDate(Date commencementDate) {
		this.commencementDate = commencementDate;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public boolean getActive() {
		return active;
	}

	public boolean hasBeenServicedThisMonth(Date toDate) {

		// TODO find more suitable ways to query for this data

		Date fromDate = DateUtils.truncate(toDate, Calendar.MONTH);
		for (Service service : services) {

			if (fromDate.before(service.getDate())
					&& toDate.after(service.getDate())) {

				return true;
			}
		}

		return false;
	}

	public boolean hasBeenServicedLastWeek(Date toDate) {
		// TODO find more suitable ways to query for this data

		Date lastWeek = DateUtils.addWeeks(toDate, -1);
		Date fromDate = ((Calendar) DateUtils.iterator(lastWeek,
				DateUtils.RANGE_WEEK_MONDAY).next()).getTime();

		for (Service service : services) {

			if (fromDate.before(service.getDate())
					&& toDate.after(service.getDate())) {

				return true;
			}
		}

		return false;
	}

	public ScheduleComment getLatestScheduleComment() {

		// TODO find better ways to do this

		Date date = null;

		ScheduleComment scheduleCommentToReturn = null;
		if (getScheduleComments().size() > 0) {

			for (ScheduleComment scheduleComment : getScheduleComments()) {

				if (date == null) {

					date = scheduleComment.getDate();
					scheduleCommentToReturn = scheduleComment;
				}

				if (scheduleComment.getDate().before(
						scheduleCommentToReturn.getDate())) {

					scheduleCommentToReturn = scheduleComment;
				}
			}
		}

		return scheduleCommentToReturn;
	}

	public Invoice getLatestInvoice() {
		// TODO find better ways of doing this

		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 1, 1);

		Invoice latestInvoice = null;

		for (Invoice invoice : invoices) {

			if(latestInvoice == null) {
				latestInvoice = invoice;
			}
			
			if (invoice.getDate().after(latestInvoice.getDate())) {

				latestInvoice = invoice;
			}
			
		}
		
		return latestInvoice;
	}
	
	public Float getBalance() {

		float balance = 0;
		for(Invoice invoice : invoices) {
			balance += invoice.getAmount();
		}
		for(Payment payment : payments) {
			balance -= payment.getAmount();
		}
		
		return balance;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
