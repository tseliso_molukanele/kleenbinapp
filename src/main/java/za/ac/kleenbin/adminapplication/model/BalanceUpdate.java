package za.ac.kleenbin.adminapplication.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BALANCE_UPDATE_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class BalanceUpdate extends Model {

	private static final long serialVersionUID = 1712778604730486785L;

	protected static final DateFormat YYYY_MM_DD = new SimpleDateFormat(
			"yyyy-MM-dd");

	@Column
	private Date date;
	@Column
	private Float amount;
	@ManyToOne
	private Account account;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return (getDate() != null ? YYYY_MM_DD.format(getDate()) : "NO DATE") + ", " + getAmount() + ", "
				+ account.getId() + ":" + account.getAccountNumber();
	}
}
