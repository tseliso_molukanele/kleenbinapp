package za.ac.kleenbin.adminapplication.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Service extends Model {

	private static final long serialVersionUID = -189631444055945783L;

	private Date date;
	
	@ManyToOne
	private Account account;
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}

}
