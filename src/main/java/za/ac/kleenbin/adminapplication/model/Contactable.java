package za.ac.kleenbin.adminapplication.model;

import java.util.List;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
public class Contactable extends Model {

	private static final long serialVersionUID = 4658222874143976282L;

	@OneToMany
	private List<PhoneNumber> phoneNumbers;
	@OneToMany
	private List<EmailAddress> emailAddresses;

	public void setEmailAddresses(List<EmailAddress> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}

	public List<EmailAddress> getEmailAddresses() {
		return emailAddresses;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
}
