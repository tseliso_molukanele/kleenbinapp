package za.ac.kleenbin.adminapplication.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.persistence.Entity;

@Entity
public class EmailAddress extends Model {

	private static final long serialVersionUID = 1L;
	
	private String email;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getEncodedEmail() {
		
		try {
			return URLEncoder.encode(email, "UTF-8").replace("%", "=");
		} catch (UnsupportedEncodingException e) {
			
			throw new RuntimeException(e);
		}
	}

}
