package za.ac.kleenbin.adminapplication.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Payment")
public class Payment extends BalanceUpdate {

	private static final long serialVersionUID = 1L;

	private String reference;

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@Override
	public String toString() {
		return super.toString() + ", " + reference;
	}

}
