package za.ac.kleenbin.adminapplication.integration;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import za.ac.kleenbin.adminapplication.exception.IntegrationException;

public class EMail {

	public class Attachment {

		public Attachment(byte[] content, String mimeType, String fileName) {
			super();
			this.content = content;
			this.mimeType = mimeType;
			this.fileName = fileName;
		}

		public byte[] getContent() {
			return content;
		}

		public String getMimeType() {
			return mimeType;
		}

		public String getFileName() {
			return fileName;
		}

		private byte[] content;
		private String mimeType;
		private String fileName;
	}

	private Session session;
	private InternetAddress fromAddress;

	public EMail(String from, Properties properties, final String username,
			final String password) throws AddressException {
		
		this.session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		
		fromAddress = new InternetAddress(from);
	}

	public void send(String[] tos, String subject,
			String messageText, Attachment... attachments) {

		try {

			MimeMessage message = new MimeMessage(session);

			message.setFrom(fromAddress);

			for (String to : tos) {

				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(to));
			}

			message.setSubject(subject);

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setText(messageText);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			for (Attachment attachment : attachments) {

				messageBodyPart = new MimeBodyPart();

				DataSource source = new ByteArrayDataSource(
						attachment.getContent(), attachment.getMimeType());

				messageBodyPart.setDataHandler(new DataHandler(source));

				messageBodyPart.setFileName(attachment.getFileName());
				multipart.addBodyPart(messageBodyPart);
			}

			message.setContent(multipart);

			Transport.send(message);
		} catch (MessagingException e) {
			throw new IntegrationException(e);
		}
	}
}
