package za.ac.kleenbin.struts2.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.struts2.util.StrutsTypeConverter;

import com.opensymphony.xwork2.conversion.TypeConversionException;

public class StringToDateConverter extends StrutsTypeConverter {

	private static final DateFormat DATEONLY_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static final DateFormat DATETIME_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");

	public Object convertFromString(@SuppressWarnings("rawtypes") Map context,
			String[] strings, @SuppressWarnings("rawtypes") Class toClass) {
		if (strings == null || strings.length == 0
				|| strings[0].trim().length() == 0) {
			return null;
		}

		try {
			
			if(strings[0].length() > 10) {
			
				return DATETIME_FORMAT.parse(strings[0]);
			} else {
				
				return DATEONLY_FORMAT.parse(strings[0]);
			}
		} catch (ParseException e) {
			throw new TypeConversionException(
					"Unable to convert given object to date: " + strings[0]);
		}
	}

	public String convertToString(@SuppressWarnings("rawtypes") Map context,
			Object date) {
		if (date != null && date instanceof Date) {
			return DATETIME_FORMAT.format(date);
		} else {
			return null;
		}
	}
}
