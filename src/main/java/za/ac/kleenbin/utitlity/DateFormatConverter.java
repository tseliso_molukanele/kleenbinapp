package za.ac.kleenbin.utitlity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateFormatConverter implements Converter {

	private static final DateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static final DateFormat DATE_FORMAT_FOR_READ = new SimpleDateFormat(
			"EEE MMM dd HH:mm:ss z yyyy");
	private static final DateFormat DATE_FORMAT_FOR_WRITE = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	@Override
	public String convert(String input) {

		try {
			if (input.trim().equals("")) {
				return DATE_FORMAT_FOR_WRITE.format(SIMPLE_DATE_FORMAT
						.parse("1990-01-01"));
			}

			return DATE_FORMAT_FOR_WRITE.format(DATE_FORMAT_FOR_READ
					.parse(input));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

}
