package za.ac.kleenbin.utitlity;

import java.io.File;

import org.springframework.transaction.annotation.Transactional;

public interface AccessDBParser {

	@Transactional
	public abstract void go(File file, boolean accountHistoryOnly, int baseYear);

}