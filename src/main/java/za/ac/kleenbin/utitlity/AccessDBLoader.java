package za.ac.kleenbin.utitlity;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import za.ac.kleenbin.adminapplication.exception.AccountNotFoundException;
import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Address;
import za.ac.kleenbin.adminapplication.model.BalanceUpdate;
import za.ac.kleenbin.adminapplication.model.BankDetails;
import za.ac.kleenbin.adminapplication.model.Business;
import za.ac.kleenbin.adminapplication.model.EmailAddress;
import za.ac.kleenbin.adminapplication.model.FREQUENCY;
import za.ac.kleenbin.adminapplication.model.GeneralComment;
import za.ac.kleenbin.adminapplication.model.Invoice;
import za.ac.kleenbin.adminapplication.model.InvoiceComment;
import za.ac.kleenbin.adminapplication.model.InvoiceCommunication;
import za.ac.kleenbin.adminapplication.model.Model;
import za.ac.kleenbin.adminapplication.model.Payment;
import za.ac.kleenbin.adminapplication.model.Person;
import za.ac.kleenbin.adminapplication.model.PhoneNumber;
import za.ac.kleenbin.adminapplication.model.PhoneNumber.NumberType;
import za.ac.kleenbin.adminapplication.model.ScheduleComment;

@Service(value = "hibernateBasedAccessDBLoader")
public class AccessDBLoader extends AbstractDBParser {

	private static final String BUSINESS_KEY = "BUSINESS_KEY";
	private static final String SESSION_META_DATA = "META_DATA";

	private static Logger logger = Logger.getLogger(AccessDBLoader.class);

	private static final DateFormat FULL_DATE_FORMAT = new SimpleDateFormat(
			"EEE MMM dd HH:mm:ss z yyyy");
	private static final DateFormat YYYY_MM_DD = new SimpleDateFormat(
			"yyyy-MM-dd");

	@PersistenceContext
	protected EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public void prepare() {

		za.ac.kleenbin.adminapplication.model.MetaData metaData = new za.ac.kleenbin.adminapplication.model.MetaData();
		metaData.setCapturedAt(new Date());

		entityManager.persist(metaData);

		getSession().put(SESSION_META_DATA, metaData);
	}

	private void extractInvoiceCommunication(String[] tokens) {

		InvoiceCommunication invoiceCommunication = new InvoiceCommunication();
		invoiceCommunication
				.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
						.get(SESSION_META_DATA));
		invoiceCommunication.setBusiness(getBusiness());
		invoiceCommunication.setMessage(tokens[0]);
		invoiceCommunication.setDate(new Date());

		if (!isEmpty(invoiceCommunication.getMessage())) {
			entityManager.persist(invoiceCommunication);
		}
	}

	private void extractPayment(String[] tokens) {
		Payment payment = new Payment();
		payment.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
				.get(SESSION_META_DATA));

		String paymentAmountString = tokens[1];
		float paymentAmount = (paymentAmountString.trim().equals("") ? 0f
				: Float.parseFloat(paymentAmountString));

		if (paymentAmount < 0.01) {

			return;
		}
		payment.setAmount(paymentAmount);

		String paymentDate = tokens[0];
		if (isEmpty(paymentDate)) {

			logger.info("Empty payment date: " + tokens[0] + ", " + tokens[1]
					+ ", " + tokens[2] + ", " + tokens[3]);
			logger.info("setting date to now");
			payment.setDate(new Date());
		} else {
			try {
				payment.setDate(FULL_DATE_FORMAT.parse(paymentDate));
			} catch (ParseException e) {

				logger.error("Could not parse date: " + tokens[0] + ", "
						+ tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
				return;
			}
		}

		String accountNumber = tokens[3];
		Account account;
		try {
			account = getAccount(accountNumber);
		} catch (AccountNotFoundException e) {
			logger.error("Skipping- could not find account: " + tokens[0]
					+ ", " + tokens[1] + ", " + tokens[2] + ", " + tokens[3]);
			return;
		}
		payment.setAccount(account);

		String paymentReference = tokens[2];
		payment.setReference(paymentReference);

		if (existForDifferentLoad(payment)) {
			return;
		}

		if (existForSameLoad(payment)) {

			logger.info("Capturing duplicate payment: " + payment);
		}
		entityManager.persist(payment);
	}

	private void extractInvoice(String[] tokens, MetaData metaData) {

		String accountNumber = tokens[0];

		Account account = null;
		try {
			account = getAccount(accountNumber);
		} catch (AccountNotFoundException e1) {
			logger.error("Account not found: " + accountNumber);
			return;
		}

		for (int index = Calendar.JANUARY; index <= Calendar.DECEMBER; index++) {

			Invoice invoice = new Invoice();
			invoice.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
					.get(SESSION_META_DATA));

			invoice.setAccount(account);

			String invoiceFrequency = tokens[1 + index * 5 + 4];
			if (isEmpty(invoiceFrequency)) {
				continue;
			}
			invoice.setServiceCount(Integer.parseInt(invoiceFrequency));

			String invoiceNumberOfBins = tokens[1 + index * 5 + 3];
			if (isEmpty(invoiceNumberOfBins)) {
				continue;
			}
			invoice.setNumberOfBins(Integer.parseInt(invoiceNumberOfBins));

			String invoiceAmountString = tokens[1 + index * 5 + 2];
			if (isEmpty(invoiceAmountString)) {
				continue;
			}

			float invoiceAmount = (invoiceAmountString.trim().equals("") ? 0f
					: Float.parseFloat(invoiceAmountString));
			invoice.setInvoiceAmount(invoiceAmount);

			invoice.setAmount(invoice.getNumberOfBins()
					* invoice.getServiceCount() * invoice.getInvoiceAmount());

			if (invoiceAmount < 0.01) {
				logger.info("Skipping zero invoice amount: " + invoice);
				continue;
			}

			String invoiceNumber = tokens[1 + index * 5 + 0];
			if (isEmpty(invoiceNumber)) {

				logger.info("Empty invoice number: " + " invNum= "
						+ invoiceNumber + ", invFrq= " + invoiceFrequency
						+ ", invNumBins= " + invoiceNumberOfBins + ", invAmt= "
						+ invoiceAmountString + ", accNum= "
						+ account.getAccountNumber());

				invoiceNumber = metaData.baseYear
						+ ((index + 1) + accountNumber);
				logger.info("invoice number has been set to:" + invoiceNumber);
			}
			invoice.setNumber(invoiceNumber);

			String invoiceDate = tokens[1 + index * 5 + 1];
			if (isEmpty(invoiceDate)) {

				logger.info("Empty invoice date: " + " invNum= "
						+ invoiceNumber + ", invFrq= " + invoiceFrequency
						+ ", invNumBins= " + invoiceNumberOfBins + ", invAmt= "
						+ invoiceAmountString + ", accNum= "
						+ account.getAccountNumber());

				Calendar calendar = Calendar.getInstance();
				calendar.setLenient(false);
				calendar.set(Calendar.YEAR, metaData.baseYear);
				calendar.set(Calendar.MONTH, index);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.HOUR_OF_DAY, 8);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);

				invoice.setDate(calendar.getTime());
				logger.info("date has been set to: "
						+ YYYY_MM_DD.format(invoice.getDate()) + " from values "
						+ metaData.baseYear + ", " + index + ", " + ", 01");
			} else {
				
				try {
					
					invoice.setDate(FULL_DATE_FORMAT.parse(invoiceDate));
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
				
			}

			if (existForDifferentLoad(invoice)) {
				return;
			}

			if (existForSameLoad(invoice)) {
				logger.info("Capturing duplicate invoice: " + invoice);
			}

			entityManager.persist(invoice);
		}
	}

	private boolean existForDifferentLoad(Invoice balanceUpdate) {

		TypedQuery<? extends BalanceUpdate> query = entityManager.createQuery(
				"from Invoice inv " + "where inv.date = :date "
						+ "AND inv.amount = :amount "
						+ "AND inv.account.accountNumber = :accountNumber "
						+ "AND inv.metaData.id != :metaDataId ", Invoice.class);
		query.setParameter("date", balanceUpdate.getDate());
		query.setParameter("amount", balanceUpdate.getAmount());
		query.setParameter("accountNumber", balanceUpdate.getAccount()
				.getAccountNumber());
		query.setParameter("metaDataId", balanceUpdate.getMetaData().getId());

		if (query.getResultList().size() > 0) {

			return true;
		}

		return false;
	}

	private boolean existForSameLoad(Invoice balanceUpdate) {

		TypedQuery<? extends BalanceUpdate> query = entityManager.createQuery(
				"from Invoice inv " + "where inv.date = :date "
						+ "AND inv.amount = :amount "
						+ "AND inv.account.accountNumber = :accountNumber "
						+ "AND inv.metaData.id = :metaDataId ", Invoice.class);
		query.setParameter("date", balanceUpdate.getDate());
		query.setParameter("amount", balanceUpdate.getAmount());
		query.setParameter("accountNumber", balanceUpdate.getAccount()
				.getAccountNumber());
		query.setParameter("metaDataId", balanceUpdate.getMetaData().getId());

		if (query.getResultList().size() > 0) {

			return true;
		}

		return false;
	}

	private boolean existForDifferentLoad(Payment balanceUpdate) {

		TypedQuery<? extends BalanceUpdate> query = entityManager.createQuery(
				"from Payment pay " + "where pay.date = :date "
						+ "AND pay.amount = :amount "
						+ "AND pay.account.accountNumber = :accountNumber "
						+ "AND pay.metaData.id != :metadataId ", Payment.class);
		query.setParameter("date", balanceUpdate.getDate());
		query.setParameter("amount", balanceUpdate.getAmount());
		query.setParameter("accountNumber", balanceUpdate.getAccount()
				.getAccountNumber());
		query.setParameter("metadataId", balanceUpdate.getMetaData().getId());

		if (query.getResultList().size() > 0) {

			return true;
		}

		return false;
	}

	private boolean existForSameLoad(Payment balanceUpdate) {

		TypedQuery<? extends BalanceUpdate> query = entityManager.createQuery(
				"from Payment pay " + "where pay.date = :date "
						+ "AND pay.amount = :amount "
						+ "AND pay.account.accountNumber = :accountNumber "
						+ "AND pay.metaData.id = :metadataId ", Payment.class);
		query.setParameter("date", balanceUpdate.getDate());
		query.setParameter("amount", balanceUpdate.getAmount());
		query.setParameter("accountNumber", balanceUpdate.getAccount()
				.getAccountNumber());
		query.setParameter("metadataId", balanceUpdate.getMetaData().getId());

		if (query.getResultList().size() > 0) {

			return true;
		}

		return false;
	}

	private Account getAccount(String accountNumber)
			throws AccountNotFoundException {
		TypedQuery<Account> query = entityManager.createQuery(
				"from Account a where a.accountNumber = ?", Account.class);
		query.setParameter(1, accountNumber);
		List<Account> results = query.getResultList();
		int resulsSize = results.size();
		if (resulsSize < 1) {
			throw new AccountNotFoundException(
					"Expected one result for account: " + accountNumber
							+ " but got " + resulsSize);
		}
		Account account = results.get(0);
		return account;
	}

	private int countAccounts(String accountNumber) {
		Query query = entityManager
				.createNativeQuery("select count(*) from Account a where a.accountNumber = ?");
		query.setParameter(1, accountNumber);
		BigInteger result = (BigInteger) query.getSingleResult();

		return result.intValue();
	}

	private Business getBusiness() {

		if (getSession().get(BUSINESS_KEY) == null) {
			getSession().put(BUSINESS_KEY,
					entityManager.find(Business.class, 1l));
		}

		return (Business) getSession().get(BUSINESS_KEY);
	}

	private void extractAccount(String[] tokens, MetaData metaData) {

		Business business = getBusiness();

		int countAcounts = countAccounts(tokens[4]);

		if (countAcounts > 0) {
			logger.info("Account " + tokens[4]
					+ " exists already and will not be recreated.");
		} else {

			List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
			if (!tokens[15].trim().equals("")) {

				PhoneNumber phoneNumber = new PhoneNumber();
				phoneNumber
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				phoneNumber.setNumber(treatPhoneNumber(tokens[15]));
				phoneNumber.setNumberType(NumberType.HOME);

				phoneNumbers.add(phoneNumber);
				entityManager.persist(phoneNumber);
			}
			if (!tokens[16].trim().equals("")) {

				PhoneNumber phoneNumber = new PhoneNumber();
				phoneNumber
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				phoneNumber.setNumber(treatPhoneNumber(tokens[16]));
				phoneNumber.setNumberType(NumberType.WORK);

				phoneNumbers.add(phoneNumber);
				entityManager.persist(phoneNumber);
			}
			if (!tokens[17].trim().equals("")) {

				PhoneNumber phoneNumber = new PhoneNumber();
				phoneNumber
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				phoneNumber.setNumber(treatPhoneNumber(tokens[17]));
				phoneNumber.setNumberType(NumberType.CELL);

				phoneNumbers.add(phoneNumber);
				entityManager.persist(phoneNumber);
			}

			List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>();

			String[] emails = tokens[18].split(",");

			for (String email : emails) {

				EmailAddress emailAddress = new EmailAddress();
				emailAddress
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				emailAddress.setEmail(email);

				if (!isEmpty(emailAddress.getEmail())) {
					emailAddresses.add(emailAddress);
					entityManager.persist(emailAddress);
				}
			}

			Person accountHolder = new Person();
			accountHolder
					.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
							.get(SESSION_META_DATA));
			accountHolder.setPhoneNumbers(phoneNumbers);
			accountHolder.setEmailAddresses(emailAddresses);
			accountHolder.setTitle(tokens[0]);
			accountHolder.setInitials(tokens[1]);
			accountHolder.setFirstName(tokens[2]);
			accountHolder.setLastName(tokens[3]);
			entityManager.persist(accountHolder);

			Address address = new Address();
			address.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
					.get(SESSION_META_DATA));
			address.setAddressLine1(tokens[11]);
			address.setStreet(tokens[12]);
			address.setSurburb(tokens[13]);
			address.setCode(tokens[14]);
			entityManager.persist(address);

			Account account = new Account();
			account.setAccountHolder(accountHolder);
			account.setAddress(address);
			account.setAccountNumber(tokens[4]);
			account.setFrequency(FREQUENCY.fromNumber(Integer
					.parseInt(tokens[5])));
			account.setNumberOfBins(Integer.parseInt(tokens[6]));
			account.setDayOfCleaning(convertToDay(tokens[7]));
			account.setSalesConsultant(tokens[9]);
			account.setActive(!metaData.isSuspended);
			account.setBusiness(business);

			if (tokens[10].trim().equals("")) {
				
				Calendar calendar = Calendar.getInstance();
				calendar.setLenient(false);
				calendar.set(Calendar.YEAR, 1990);
				calendar.set(Calendar.MONTH, 0);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.HOUR_OF_DAY, 8);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				
				account.setCommencementDate(calendar.getTime());
			} else {

				try {
					account.setCommencementDate(FULL_DATE_FORMAT
							.parse(tokens[10]));
				} catch (ParseException e) {
					throw new RuntimeException(e);
				}
			}

			List<ScheduleComment> scheduleComments = new ArrayList<ScheduleComment>();
			if (tokens[19].trim().equals("")) {
				ScheduleComment scheduleComment = new ScheduleComment();
				scheduleComment
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				scheduleComment.setComment(tokens[19]);
				scheduleComments.add(scheduleComment);

				if (!isEmpty(scheduleComment.getComment())) {
					entityManager.persist(scheduleComment);
					account.setScheduleComments(scheduleComments);
				}
			}

			List<InvoiceComment> invoiceComments = new ArrayList<InvoiceComment>();
			if (tokens[20].trim().equals("")) {
				InvoiceComment invoiceComment = new InvoiceComment();
				invoiceComment
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				invoiceComment.setComment(tokens[20]);
				invoiceComments.add(invoiceComment);

				if (!isEmpty(invoiceComment.getComment())) {
					entityManager.persist(invoiceComment);
					account.setInvoiceComments(invoiceComments);
				}
			}
			List<GeneralComment> generalComments = new ArrayList<GeneralComment>();
			if (tokens[21].trim().equals("")) {
				GeneralComment generalComment = new GeneralComment();
				generalComment
						.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
								.get(SESSION_META_DATA));
				generalComment.setComment(tokens[21]);
				generalComments.add(generalComment);

				if (!isEmpty(generalComment.getComment())) {
					entityManager.persist(generalComment);
					account.setGeneralComments(generalComments);
				}
			}

			entityManager.persist(account);
		}
	}

	private String treatPhoneNumber(String number) {
		
		String treated = number.replaceAll("[\\(\\)\\s]+", "");
		
		if(!treated.matches("\\d{10}")) {
			throw new RuntimeException("Phone number is not useful: " + number + " -> " + treated);
		}
		
		return treated;
	}

	protected void extractFromLine(String line,
			Class<? extends Model> targetClass, MetaData metaData) {

		if (Business.class.equals(targetClass)) {

			String[] tokens = line.split("\\|");
			extractBusiness(tokens);
		} else if (Account.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 22);
			extractAccount(tokens, metaData);
		} else if (InvoiceCommunication.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 1);
			extractInvoiceCommunication(tokens);
		} else if (Invoice.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 61);
			extractInvoice(tokens, metaData);
		} else if (Payment.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 4);
			extractPayment(tokens);
		}
	}

	private void extractBusiness(String[] tokens) {
		Business business = new Business();
		business.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
				.get(SESSION_META_DATA));

		business.setCompanyNumber(tokens[0]);
		business.setTradingAs(tokens[1]);
		business.setFranchiseArea(tokens[2]);

		Address address = new Address();
		address.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
				.get(SESSION_META_DATA));
		address.setAddressLine1(tokens[6]);
		address.setStreet(tokens[7]);
		address.setSurburb(tokens[8]);
		address.setCode(tokens[9]);

		entityManager.persist(address);

		BankDetails bankDetails = new BankDetails();
		bankDetails
				.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
						.get(SESSION_META_DATA));
		bankDetails.setBank(tokens[14]);
		bankDetails.setAccountName(tokens[15]);
		bankDetails.setBranchName(tokens[16]);
		bankDetails.setBranchCode(tokens[17]);
		bankDetails.setAccountNumber(tokens[18]);

		entityManager.persist(bankDetails);

		List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();

		if (!isEmpty(tokens[10])) {
			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber
					.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
							.get(SESSION_META_DATA));
			phoneNumber.setNumber(treatPhoneNumber(tokens[10]));
			phoneNumber.setNumberType(NumberType.WORK);

			entityManager.persist(phoneNumber);
			phoneNumbers.add(phoneNumber);
		}

		if (!isEmpty(tokens[11])) {
			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber
					.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
							.get(SESSION_META_DATA));
			phoneNumber.setNumber(treatPhoneNumber(tokens[11]));
			phoneNumber.setNumberType(NumberType.CELL);

			entityManager.persist(phoneNumber);
			phoneNumbers.add(phoneNumber);
		}

		if (!isEmpty(tokens[12])) {
			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber
					.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
							.get(SESSION_META_DATA));
			phoneNumber.setNumber(treatPhoneNumber(tokens[12]));
			phoneNumber.setNumberType(NumberType.FAX);

			entityManager.persist(phoneNumber);
			phoneNumbers.add(phoneNumber);
		}

		List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>();
		EmailAddress emailAddress = new EmailAddress();
		emailAddress
				.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
						.get(SESSION_META_DATA));
		emailAddress.setEmail(tokens[13]);
		entityManager.persist(emailAddress);

		emailAddresses.add(emailAddress);

		business.setAddress(address);
		business.setEmailAddresses(emailAddresses);
		business.setPhoneNumbers(phoneNumbers);
		business.setMembers(new ArrayList<Person>());
		business.setBankDetails(bankDetails);

		for (int i = 3; i <= 5; i++) {

			String[] nameInfo = tokens[i].trim().split(" ");

			if (nameInfo.length > 1) {

				Person person = new Person();
				person.setMetaData((za.ac.kleenbin.adminapplication.model.MetaData) getSession()
						.get(SESSION_META_DATA));

				person.setFirstName(nameInfo[0]);
				person.setLastName(nameInfo[1]);

				business.getMembers().add(person);
				entityManager.persist(person);
			}
		}

		entityManager.persist(business);
	}

	private int convertToDay(String input) {

		if (input.trim().toUpperCase().equals("SUNDAY")) {
			return Calendar.SUNDAY;

		} else if (input.trim().toUpperCase().equals("MONDAY")) {
			return Calendar.MONDAY;

		} else if (input.trim().toUpperCase().equals("TUESDAY")) {

			return Calendar.TUESDAY;
		} else if (input.trim().toUpperCase().equals("WEDNESDAY")) {

			return Calendar.WEDNESDAY;
		} else if (input.trim().toUpperCase().equals("THURSDAY")) {

			return Calendar.THURSDAY;
		} else if (input.trim().toUpperCase().equals("FRIDAY")) {

			return Calendar.FRIDAY;
		} else if (input.trim().toUpperCase().equals("SATURDAY")) {

			return Calendar.SATURDAY;
		} else if (input.trim().toUpperCase().equals("")) {

			return Calendar.SUNDAY;
		} else {
			throw new RuntimeException("Cannot convert: " + input);
		}
	}

	private boolean isEmpty(String... arguments) {

		for (String argument : arguments) {

			if (argument == null) {

				return true;
			}

			if (argument.trim().equals("")) {

				return true;
			}
		}

		return false;
	}
}
