package za.ac.kleenbin.utitlity;

public class TrimConverter implements Converter {

	@Override
	public String convert(String input) {

		if(input == null || input.trim().equals("")) {
			
			return "";
		}
		
		return input.trim();
	}

}
