package za.ac.kleenbin.utitlity;

public class PhoneNumberConverter implements Converter {

	@Override
	public String convert(String input) {
		
		input = input.replace(" ", "");
		input = input.replace("(", "");
		input = input.replace(")", "");
		input = input.trim();
		
		return input;
	}

}
