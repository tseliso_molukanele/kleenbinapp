package za.ac.kleenbin.utitlity.reader;

public interface LineReader {
	
	String readLine();
	void close();
}
