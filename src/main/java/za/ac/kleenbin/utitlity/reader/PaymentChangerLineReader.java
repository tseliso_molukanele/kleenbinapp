package za.ac.kleenbin.utitlity.reader;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PaymentChangerLineReader implements LineReader {

	private static final SimpleDateFormat DATE_FORMAT_FOR_READ = new SimpleDateFormat(
			"EEE MMM dd HH:mm:ss z yyyy");
	private static final SimpleDateFormat DATE_FORMAT_FOR_WRITE = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private LineReader lineReader;

	public PaymentChangerLineReader(LineReader lineReader) {

		this.lineReader = lineReader;
	}

	@Override
	public String readLine() {

		String originalLine = lineReader.readLine();

		if (originalLine == null) {

			return null;
		}

		String[] tokens = originalLine.split("\\|");

		String[] newColumns = new String[4];

		if (tokens[1].trim().equals("") || new Float(tokens[1]) < 0.001) {

			return "";
		}

		try {

			newColumns[0] = ("".equals(tokens[0]) ? "2010-12-31 23:59:59" : DATE_FORMAT_FOR_WRITE.format(DATE_FORMAT_FOR_READ
					.parse(tokens[0])));
			newColumns[1] = "-" + tokens[1];
			newColumns[2] = tokens[2];
			newColumns[3] = tokens[3];
		} catch (ParseException e) {

			throw new RuntimeException("problem with: " + originalLine, e);
		}

		String toReturn = "";
		for (int i = 0; i < 4; i++) {

			toReturn += newColumns[i];
			if (i < 3) {
				toReturn += "|";
			}
		}

		return toReturn;
	}

	@Override
	public void close() {

		lineReader.close();
	}

}
