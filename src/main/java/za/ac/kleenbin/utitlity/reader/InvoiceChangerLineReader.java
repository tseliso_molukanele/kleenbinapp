package za.ac.kleenbin.utitlity.reader;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class InvoiceChangerLineReader implements LineReader {

	private static final SimpleDateFormat DATE_FORMAT_FOR_READ = new SimpleDateFormat(
			"EEE MMM dd HH:mm:ss z yyyy");
	private static final SimpleDateFormat DATE_FORMAT_FOR_WRITE = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private LineReader lineReader;

	public InvoiceChangerLineReader(LineReader lineReader) {

		this.lineReader = lineReader;
	}

	@Override
	public String readLine() {

		String originalLine = lineReader.readLine();

		if (originalLine == null) {

			return null;
		}

		String[] originalColumns = originalLine.split("\\|");

		String[] newColumns = new String[9];
		try {

			if (originalColumns[2].trim().equals("")
					|| originalColumns[3].trim().equals("")
					|| originalColumns[4].trim().equals("")
					|| originalColumns[5].trim().equals("")) {

				return "";
			}

			newColumns[0] = (originalColumns[1].trim().equals("") ? "2010-12-31 23:59:59" : DATE_FORMAT_FOR_WRITE.format(DATE_FORMAT_FOR_READ.parse(originalColumns[1])));
			newColumns[1] = ""
					+ (Float.parseFloat(originalColumns[2])
							* Float.parseFloat(originalColumns[3]) * Float
							.parseFloat(originalColumns[4]));
			newColumns[2] = originalColumns[4];
			newColumns[3] = originalColumns[2];
			Calendar calendar = Calendar.getInstance();
			calendar.setTime((originalColumns[1].trim().equals("") ? DATE_FORMAT_FOR_WRITE.parse("2010-12-31 23:59:59") : DATE_FORMAT_FOR_READ.parse(originalColumns[1])));
			newColumns[4] = "" + calendar.get(Calendar.MONTH);
			newColumns[5] = originalColumns[0];
			newColumns[6] = originalColumns[3];
			newColumns[7] = "" + calendar.get(Calendar.YEAR);

			newColumns[8] = originalColumns[5];
		} catch (Exception e) {

			String additionalInfo = "[";
			
			for(String string : originalColumns) {
				
				additionalInfo += string + ",";
			}
			
			additionalInfo += "]";
			
			throw new RuntimeException("problem with: " + originalLine + "= " + additionalInfo, e);
		}

		String toReturn = "";

		for (int i = 0; i < 9; i++) {

			toReturn += newColumns[i];
			if (i < 8) {
				toReturn += "|";
			}
		}

		return toReturn;
	}

	@Override
	public void close() {

		lineReader.close();
	}

}
