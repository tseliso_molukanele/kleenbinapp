package za.ac.kleenbin.utitlity.reader;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;

public class JackAccessLineReader implements LineReader {

	private Iterator<Map<String, Object>> iterator;
	private String[] columns;

	public JackAccessLineReader(Table table, String[] columns) {

		iterator = table.iterator();
		this.columns = columns;
	}

	@Override
	public String readLine() {

		if (iterator.hasNext()) {

			Map<String, Object> row = iterator.next();
			StringBuilder returnable = new StringBuilder();

			for (int i = 0; i < columns.length; i++) {

				if (row.get(columns[i]) != null) {

					returnable.append(row.get(columns[i]));
				}

				if (i < columns.length - 1) {
					returnable.append("|");
				}
			}

			return returnable.toString();
		} else {

			return null;
		}
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	public static LineReader getReader(File file, String tableName,
			String... columns) {

		try {
			Table table = Database.open(file, true).getTable(tableName);

			return new JackAccessLineReader(table, columns);

		} catch (IOException e) {

			throw new RuntimeException(e);
		}

	}
}
