package za.ac.kleenbin.utitlity.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileLineReader implements LineReader {

	private BufferedReader bufferedReader; 
	
	public FileLineReader(String file) {

		try {

			bufferedReader = new BufferedReader(new FileReader(
					file));
		} catch (FileNotFoundException e) {

			throw new RuntimeException(e);
		}
	}

	@Override
	public String readLine() {
		try {
			
			return bufferedReader.readLine();
		} catch (IOException e) {
			
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() {
		try {
			bufferedReader.close();
		} catch (IOException e) {
			
			throw new RuntimeException(e);
		}
	}

}
