package za.ac.kleenbin.utitlity;

import java.util.Calendar;

public class DateDayConverter implements Converter {

	@Override
	public String convert(String input) {

		if (input.trim().toUpperCase().equals("SUNDAY")) {
			return "" + Calendar.SUNDAY;

		} else if (input.trim().toUpperCase().equals("MONDAY")) {
			return "" + Calendar.MONDAY;

		} else if (input.trim().toUpperCase().equals("TUESDAY")) {

			return "" + Calendar.TUESDAY;
		} else if (input.trim().toUpperCase().equals("WEDNESDAY")) {

			return "" + Calendar.WEDNESDAY;
		} else if (input.trim().toUpperCase().equals("THURSDAY")) {

			return "" + Calendar.THURSDAY;
		} else if (input.trim().toUpperCase().equals("FRIDAY")) {

			return "" + Calendar.FRIDAY;
		} else if (input.trim().toUpperCase().equals("SATURDAY")) {

			return "" + Calendar.SATURDAY;
		} else if (input.trim().toUpperCase().equals("")) {

			return "" + Calendar.SUNDAY;
		} else {
			throw new RuntimeException("Cannot convert: " + input);
		}
	}
}
