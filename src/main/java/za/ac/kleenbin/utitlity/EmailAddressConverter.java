package za.ac.kleenbin.utitlity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailAddressConverter implements Converter {

	private Pattern pattern;
	private Matcher matcher;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public EmailAddressConverter() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	@Override
	public String convert(String input) {

		input = input.trim();

		if (validate(input)) {

			return input;
		}

		return "";
	}

	public boolean validate(final String hex) {

		matcher = pattern.matcher(hex);
		return matcher.matches();

	}

}
