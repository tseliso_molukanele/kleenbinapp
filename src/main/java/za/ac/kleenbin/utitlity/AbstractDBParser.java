package za.ac.kleenbin.utitlity;

import static za.ac.kleenbin.utitlity.reader.JackAccessLineReader.getReader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Business;
import za.ac.kleenbin.adminapplication.model.Invoice;
import za.ac.kleenbin.adminapplication.model.InvoiceCommunication;
import za.ac.kleenbin.adminapplication.model.Model;
import za.ac.kleenbin.adminapplication.model.Payment;
import za.ac.kleenbin.utitlity.reader.LineReader;

public abstract class AbstractDBParser implements AccessDBParser {

	protected final class MetaData {

		public MetaData() {

		}

		public MetaData setIsSuspended(boolean isSuspended) {

			this.isSuspended = isSuspended;
			return this;
		}

		boolean isSuspended = false;
		int baseYear;

		public MetaData setBaseYear(int baseYear) {
			
			this.baseYear = baseYear;
			return this;
		}
	}

	private static Logger logger = Logger.getLogger(AbstractDBParser.class);

	public AbstractDBParser() {
		super();
	}

	private Map<String, Object> session;

	public Map<String, Object> getSession() {
		return session;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.ac.kleenbin.utitlity.AccessDBImporter#go(java.io.File)
	 */
	@Override
	@Transactional
	public void go(File file, boolean accountHistoryOnly, int baseYear) {

		logger.info("Starting extract for file: " + file.getName());
		session = new HashMap<String, Object>();
		prepare();

		if (!accountHistoryOnly) {
			extractBusiness(file);
			extractAccounts(file);
			extractComments(file);
		}

		extractInvoices(file, baseYear);
		extractPayment(file);

		close();
		session.clear();
		logger.info("Finished extract for file: " + file.getName());
	}

	private void extractPayment(File file) {
		logger.debug("Entered extractPayment()");

		for (String readTable : new String[] { "Payment",
				"Payment archive table" }) {

			LineReader lineReader = getReader(file, readTable, "Payment Date",
					"Payment Amount", "Payment Reference", "Contract No");

			String line = lineReader.readLine();
			while (line != null) {

				extractFromLine(line, Payment.class, null);
				line = lineReader.readLine();
			}
		}
	}

	private void extractBusiness(File file) {
		logger.debug("Entered extractBusiness()");

		LineReader lineReader = getReader(file, "Owners Information",
				"CC or Company no", "Trading as", "Francise Area", "Member 1",
				"Member 2", "Member 3", "Address 1", "Address 2", "Address 3",
				"Address 4", "Company Telephone Number", "Cell No",
				"Company Fax Number", "E-Mail Address", "Bank", "Account Name",
				"Branch", "Branch Code", "Account Number");

		String line = lineReader.readLine();

		extractFromLine(line, Business.class, null);
	}

	private void extractAccounts(File file) {
		logger.debug("Entered extractAccounts()");

		for (String readTable : new String[] { "customer database",
				"Suspended Table" }) {

			boolean isSuspended = readTable.toLowerCase().contains("suspend");

			LineReader lineReader = getReader(file, readTable,

			"Clients Title", "Clients Initials", "Client First Name",
					"Clients Surname", "Contract no", "Frequency",
					"No of Bins to be cleaned", "Day of Cleaning",
					"Outstanding from previouse year", "Sales Consultant",
					"Date of Commencement", "Service Address Street No",
					"Service Address Street Name", "Service Address Suburb",
					"Service Address Postal Code", "Clients HomeTe No",
					"Clients Work Tel No", "Clients Cell no", "Company Vat No",
					"Comments", "Comments for invoice", "General Comments");

			String line = lineReader.readLine();

			while (line != null) {

				extractFromLine(line, Account.class,
						new MetaData().setIsSuspended(isSuspended));
				line = lineReader.readLine();
			}
		}
	}

	private void extractComments(File file) {
		logger.debug("Entered extractComments()");

		LineReader lineReader = getReader(file, "Invoice comments",
				"Invoice Comments");

		String line = lineReader.readLine();
		while (line != null) {

			extractFromLine(line, InvoiceCommunication.class, null);
			line = lineReader.readLine();
		}
	}

	private void extractInvoices(File file, int baseYear) {
		logger.debug("Entered extractInvoices()");

		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(baseYear, "Invoice");
		map.put(baseYear - 1, "Invoice archive table");
		
		for(Integer year : map.keySet()) {

			LineReader lineReader = getReader(file, map.get(year), "Contract No",
					"JanInvNo", "JanInvDate", "JanInvAmt", "JanNoOfBins",
					"JanFreq", "FebInvNo", "FebInvDate", "FebInvAmt",
					"FebNoOfBins", "FebFreq", "MarInvNo", "MarInvDate",
					"MarInvAmt", "MarNoOfBins", "MarFreq", "AprInvNo",
					"AprInvDate", "AprInvAmt", "AprNoOfBins", "AprFreq",
					"MayInvNo", "MayInvDate", "MayInvAmt", "MayNoOfBins",
					"MayFreq", "JunInvNo", "JunInvDate", "JunInvAmt",
					"JunNoOfBins", "JunFreq", "JulInvNo", "JulInvDate",
					"JulInvAmt", "JulNoOfBins", "JulFreq", "AugInvNo",
					"AugInvDate", "AugInvAmt", "AugNoOfBins", "AugFreq",
					"SepInvNo", "SepInvDate", "SepInvAmt", "SepNoOfBins",
					"SepFreq", "OctInvNo", "OctInvDate", "OctInvAmt",
					"OctNoOfBins", "OctFreq", "NovInvNo", "NovInvDate",
					"NovInvAmt", "NovNoOfBins", "NovFreq", "DecInvNo",
					"DecInvDate", "DecInvAmt", "DecNoOfBins", "DecFreq");

			String line = lineReader.readLine();
			while (line != null) {

				extractFromLine(line, Invoice.class, new MetaData().setBaseYear(year));
				line = lineReader.readLine();
			}
		}
	}

	protected abstract void extractFromLine(String line,
			Class<? extends Model> klass, MetaData metaData);

	protected void prepare() {
	}

	protected void close() {
		
	}
}