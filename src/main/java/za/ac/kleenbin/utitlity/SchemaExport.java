package za.ac.kleenbin.utitlity;

import java.util.Set;

import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.reflections.Reflections;

import za.ac.kleenbin.adminapplication.model.MetaData;
import za.ac.kleenbin.adminapplication.model.Model;

public class SchemaExport {

	public static void main(String[] args) {

		Configuration configuration = new Configuration();

		Reflections reflections = new Reflections(
				"za.ac.kleenbin.adminapplication.model");
		Set<Class<? extends Model>> classes = reflections
				.getSubTypesOf(Model.class);

		for (Class<? extends Model> clazz : classes) {

			configuration.addAnnotatedClass(clazz);
		}
		configuration.addAnnotatedClass(MetaData.class);

		configuration.setProperty(Environment.DIALECT,
				"org.hibernate.dialect.MySQL5Dialect");

		org.hibernate.tool.hbm2ddl.SchemaExport schema = new org.hibernate.tool.hbm2ddl.SchemaExport(
				configuration);
		schema.setOutputFile("etc/sql/nondata/schema.sql");

		schema.create(false, false);

	}

}
