package za.ac.kleenbin.utitlity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.Calendar;

import za.ac.kleenbin.adminapplication.model.Account;
import za.ac.kleenbin.adminapplication.model.Business;
import za.ac.kleenbin.adminapplication.model.Invoice;
import za.ac.kleenbin.adminapplication.model.InvoiceCommunication;
import za.ac.kleenbin.adminapplication.model.Model;
import za.ac.kleenbin.adminapplication.model.Payment;

public class AccessDBtoCVSExporter extends AbstractDBParser {

	public static void main(String[] args) throws URISyntaxException,
			FileNotFoundException {

		PrintWriter invoicesWriter = new PrintWriter(
				"/home/tseliso/temp/KB/discrepancies/invoices.csv");

		AccessDBtoCVSExporter exporter = new AccessDBtoCVSExporter(
				invoicesWriter);

		exporter.go(
				new File(
						"/home/tseliso/temp/KB/discrepancies/KleenBin-CLEANDB-4.5.mdb"),
				false, 2013);
		exporter.go(
				new File(
						"/home/tseliso/temp/KB/discrepancies/KleenBin-CLEANDB-4.5-BEFOREAA.mdb"),
				true, 2012);
		invoicesWriter.close();
	}

	private PrintWriter invoicesWriter;

	public AccessDBtoCVSExporter(PrintWriter invoicesWriter) {

		this.invoicesWriter = invoicesWriter;
	}

	@Override
	protected void extractFromLine(String line,
			Class<? extends Model> targetClass, MetaData metaData) {

		if (Business.class.equals(targetClass)) {

			String[] tokens = line.split("\\|");
		} else if (Account.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 22);
		} else if (InvoiceCommunication.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 1);
		} else if (Invoice.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 61);
			extractInvoice(tokens);
		} else if (Payment.class.equals(targetClass)) {

			String[] tokens = line.split("\\|", 4);
		}
	}

	private void extractInvoice(String[] tokens) {

		String accountNumber = tokens[0];

		for (int i = Calendar.JANUARY; i <= Calendar.DECEMBER; i++) {

			String invoiceNumber = tokens[1 + i * 5 + 0];
			String invoiceFrequency = tokens[1 + i * 5 + 4];
			String invoiceNumberOfBins = tokens[1 + i * 5 + 3];
			String invoiceAmountString = tokens[1 + i * 5 + 2];
			String invoiceDate = tokens[1 + i * 5 + 1];

			invoicesWriter.println(accountNumber + ", " + invoiceNumber + ", "
					+ invoiceFrequency + ", " + invoiceNumberOfBins + ", "
					+ invoiceAmountString + ", " + invoiceDate);

		}
	}

}
