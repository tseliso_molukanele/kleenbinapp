package za.ac.kleenbin.utitlity;

import za.ac.kleenbin.adminapplication.model.FREQUENCY;

public class FrequencyConverter implements Converter {

	@Override
	public String convert(String input) {

		return ""
				+ FREQUENCY.fromNumber(Integer.parseInt(input.trim()))
						.ordinal();
	}

}
