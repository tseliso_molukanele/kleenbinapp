package za.ac.kleenbin.utitlity;

public class ConstantConverter implements Converter {

	private String constant;
	public ConstantConverter(String constant) {
		
		this.constant = constant;
	}
	
	@Override
	public String convert(String input) {
		return constant;
	}

}
